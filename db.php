<?php

$servername = getenv('IP');
    $username = getenv('C9_USER');
    $password = "";
    $database = "c9";
    $dbport = 3306;

    // Create connection
    $db = new mysqli($servername, $username, $password, $database, $dbport);

    // Check connection
    if ($db->connect_error) {
        die("Connection failed: " . $db->connect_error."<br>");
    } 
    echo "Connected successfully (".$db->host_info.")<br>";
    
$sql = "CREATE TABLE BRANDS(
BRAND_ID INT(10) UNSIGNED NOT NULL ,
NAME VARCHAR(60) NOT NULL,
EXISTENCE VARCHAR(1) DEFAULT '1',
UNIQUE (NAME),
PRIMARY KEY (BRAND_ID)
)";

if ($db->query($sql) === TRUE) {
    echo "Table BRANDS created successfully<br>";
} else {
    echo "***Error creating table BRANDS: " . $db->error."<br>";
}
?>